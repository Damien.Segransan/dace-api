from __future__ import annotations
import requests
import numpy as np


def get_stellar_properties(target: str) -> tuple[float, float, float]:
    """
    Retrieve the proper motion (Right ascension, Declination) and parallax value (using simbad) for the specified target.

    :param target: The target to retrieve stellar properties from
    :type target: str
    :return: The proper motion (Right ascension, Declination) and the parallax value
    :rtype: tuple[float, float, float]

    >>> from dace_query.helpers.helpers import get_stellar_properties
    >>> pm_ra, pm_dec, plx_mas = get_stellar_properties('HD 335006')
    """
    simbad_url = "https://simbad.cds.unistra.fr/simbad/sim-script"

    # This simbad script does:
    # ... disable console, error and script output to get only the data output.
    # ... create a format object call secular-information and use it.
    # ... The format : Proper Motion (Right ascension axis, Declination axis ) + Parallax value
    # ... query the specified target

    raw_script = f"""output console=off error=off script=off
    format object secular-information "%PM(A|D)|%PLX(V)"
    {target}
    """
    # Retrieve information from simbad
    try:
        response = requests.get(
            url=simbad_url,
            params={'script': raw_script}
        )
        response.raise_for_status()
    except requests.exceptions.RequestException as e:
        raise e

    # Parse the retrieved data
    try:
        values = response.text.strip().split('|')
        pm_ra, pm_dec, plx_mas = list(map(lambda value: float(value.strip()), values))
        return pm_ra, pm_dec, plx_mas
    except Exception as e:
        raise e


def compute_secular_acceleration(rv: np.ndarray, bjd: np.ndarray, pm_ra: float, pm_dec: float,
                                 plx_mas: float) -> np.ndarray:
    """
    Compute the secular acceleration.

    :param rv: The radial velocity vector
    :type rv: np.ndarray
    :param bjd: The barycentric julian date vector
    :type bjd: np.ndarray
    :param pm_ra: The proper motion right ascension
    :type pm_ra: float
    :param pm_dec: The proper motion declination
    :type pm_dec: float
    :param plx_mas: The parallax value
    :type plx_mas: float
    :return: The radial velocity corrected vector
    :rtype: np.ndarray

    >>> from dace_query.helpers.helpers import compute_secular_acceleration, get_stellar_properties
    >>> import numpy as np
    >>> bjd = np.array([50862.3754, 50887.3556, 51156.5091])
    >>> rv = np.array([12179.9, 12189.7, 12188.1])
    >>> rv_corrected = compute_secular_acceleration(rv, bjd, *get_stellar_properties('HD31253'))
    """
    d_m = 1000.0 / plx_mas * 3.08567758e16
    mu_rad_s = np.sqrt(pm_ra * pm_ra + pm_dec * pm_dec) * 2 * np.pi / (360.0 * 1000.0 * 3600.0 * 86400.0 * 365.25)
    secular_acceleration = d_m * mu_rad_s * mu_rad_s * 86400.0
    return rv - ((bjd - bjd.mean()) * secular_acceleration)
