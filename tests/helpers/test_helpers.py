import pytest
from dace_query.helpers.helpers import get_stellar_properties


@pytest.mark.parametrize('target, expected_values', [
    pytest.param('HD31253', (90.128, -42.736, 17.1155)),
    pytest.param('51peg', (207.328, 61.164, 64.4048))
])
def test_get_stellar_properties(target, expected_values):
    assert get_stellar_properties(target) == expected_values
