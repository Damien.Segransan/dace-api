from dace_query.helpers.helpers import compute_secular_acceleration, get_stellar_properties
import numpy as np

x = get_stellar_properties('HD31253')
print(x)
bjd = np.array([50862.3754, 50887.3556, 51156.5091])
rv = np.array([12179.9, 12189.7, 12188.1])
rv_corrected = compute_secular_acceleration(rv, bjd, *get_stellar_properties('HD31253'))

y = get_stellar_properties('51peg')
print(y)