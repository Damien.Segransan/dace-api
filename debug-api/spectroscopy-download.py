from pathlib import Path

from dace_query import DaceClass
from dace_query.spectroscopy import SpectroscopyClass, Spectroscopy

fp_config = Path('../tests/config.ini')
fp_dacerc = Path('../tests/dev.dacerc')

instance = SpectroscopyClass(
    dace_instance=DaceClass(
        config_path=fp_config,
        dace_rc_config_path=fp_dacerc))

instance.download_files(['harps/DRS-3.5/reduced/2019-07-05/HARPS.2019-07-06T04:00:00.322.fits'],
                        file_type='s1d',
                        output_filename='r.tar.gz', output_directory='/tmp')

instance.download_files(
    files=['coralie14/DRS-3.8/reduced/2017-08-05/CORALIE.2017-08-06T10:12:09.000.fits'],
    file_type='guidance',
    output_filename='guidance.tar.gz',
    output_directory='/tmp'
)

"""
# When file will be available
instance.download_files(
    files=['espresso/DRS-3.0.0/reduced/2023-02-06/r.ESPRE.2023-02-07T03:23:37.984.fits'],
    file_type='guidance',
    output_filename='espresso-guidance.tar.gz',
    output_directory='/tmp'
)
"""
fits_file_to_download = 'harpn/DRS-3.7/reduced/2013-04-15/HARPN.2013-04-16T02-30-43.654.fits'

Spectroscopy.get_guiding_file(
    fits_file=fits_file_to_download,
    output_directory='/tmp',
    output_filename='guiding.fits'
)
