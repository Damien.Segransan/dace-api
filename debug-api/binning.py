from dace_query.spectroscopy import Spectroscopy
from dace_query.helpers import algorithm
import pandas as pd

# Retrieve the data of 51 Peg
raw_dataframe: pd.DataFrame = Spectroscopy.get_timeseries(
    target='51Peg',
    sorted_by_instrument=False,
    output_format='pandas')

print(raw_dataframe.columns)

# Create a subset dataframe with important/chosen columns
dataframe = raw_dataframe[
    ['rv', 'rv_err', 'fwhm', 'fwhm_err', 'rjd', 'cal_therror', 'ins_name', 'cal_thfile', 'date_night','prog_id',
     'raw_file']].copy()

# Fill NaN value
dataframe['fwhm'].fillna(value=0.0, inplace=True)
dataframe['fwhm_err'].fillna(value=0.0, inplace=True)

binned_df = algorithm.binning(dataframe, group_by=['ins_name', 'prog_id', 'cal_thfile', 'date_night'], binning_time=0.7)

print(binned_df[['rv', 'rv_err']].to_numpy())
print(binned_df['rv'].to_numpy())
