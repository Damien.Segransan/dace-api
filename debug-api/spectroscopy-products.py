from pathlib import Path

from dace_query import DaceClass
from dace_query.spectroscopy import SpectroscopyClass
import re

fp_config = Path('../tests/config.ini')
fp_dacerc = Path('../tests/dev.dacerc')

instance = SpectroscopyClass(
    dace_instance=DaceClass(
        config_path=fp_config,
        dace_rc_config_path=fp_dacerc
    )
)

#filters = {'tpl_id': {'contains': ['obs', 'sun', 'sol']}}
filters = {}
data = instance.query_calibrations(filters=filters, output_format='dict', limit=1)
print(data['file_rootpath'])
# instance.download_product_files(files=data['file_rootpath'], output_directory='/tmp', output_filename='a.tar.gz')

