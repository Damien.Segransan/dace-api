Usage examples
##############

Filtering bad quality data
**************************

Before applying any scientific operation on the recovered data, it is important to ensure their good quality and therefore to filter out the bad ones.

There is not like on the `DACE website <https://dace.unige.ch>`_ a filter doing it automatically for you, with dace-query you will have to do it **manually**.

To do so, it is necessary to check the following parameters :

* The DRS quality check : ``drs_qc``
* The error on the radial velocity : ``rv_err``
* The Signal to Noise at order 50 of the CCD : ``spectroFluxSn50``


**Here is an example filtering bad quality data to keep only the good ones :**

.. code-block:: python

    from dace_query.spectroscopy import Spectroscopy
    import numpy as np

    # Retrieve radial velocity timeseries for the HD40307 target
    result = Spectroscopy.get_timeseries(target='HD40307', sorted_by_instrument=False, output_format='numpy')

    # Filter based on :
    # * the radial velocities error
    # * the drs quality check
    # * the signal to noise at order 50 (if available).
    condition = (result['rv_err'] > 0.0) & (result['rv_err'] < 20.0) & \
            (result['drs_qc']) & \
            ((np.isnan(result['spectroFluxSn50'])) | (result['spectroFluxSn50'] > 20))

    # Apply the filter on the retrieved data
    filtered_data = dict(map(lambda pair: (pair[0], pair[1][condition]), result.items()))

    #  ... the equivalent more human-readable code
    filtered_data = {}
    for parameter, values in result.items():
        filtered_data[parameter] = values[condition]

Download non common data products from a CHEOPS visit
******************************************************

On the `DACE website <https://dace.unige.ch>`_, several data products per visit can be downloaded, such as light curves, images or many other types of files already defined.

But it is also possible for each of the cheops visits to **list all the data products**, to **select some non common** (like the Attitude, HkCentroid, ...) and then to **download them**.

With the dace-query package, this last one can be achieved thanks to the combination of different functions :

* ``Cheops.list_data_product(visit_filepath=...)``
* ``Cheops.download_files(file_type='files', files=...)``
* and some regular expressions using ``import re``

**Here is an example to download the "attitude" product files of a CHEOPS visit for the star HD88111 :**

.. code-block:: python

    import re
    from pathlib import Path
    from dace_query.cheops import Cheops

    # Retrieve one cheops visit for the target HD88111
    cheops_visits = Cheops.query_database(
        filters={
            'obj_id_catname': {
                'equal': ['HD88111']
            }
        },
        limit=1,
        output_format='dict')

    for visit in cheops_visits.get('file_rootpath', []):

        # Get all data products available for the raw file specified (=visit)
        visit_products = Cheops.list_data_product(
            visit_filepath=str(visit),
            output_format='dict'
        )

        # ... check if there are data products
        if (not visit_products) and visit_products.get('file', None):
            continue  # pass loop # log...


        # Get files containing "Attitude" in their name using regex
        files = [product for product in visit_products['file'] if
                re.match('.*Attitude.*', Path(product).name, re.IGNORECASE)]

        # ... check if there are files matching the regex
        if not files:
            continue  # pass loop # log...


        # Download the matched files
        Cheops.download_files(
            files=files,
            file_type='files',
            output_directory='/tmp',
            output_filename=f'{Path(visit).parent.name}.tar.gz'
        )

Apply binning on a dataframe
****************************

When there is a multitude of radial velocity observations per night for a star, it becomes important to group and **reduce these points to a single point** in order **to best analyse the data**.

In order **to carry out such a grouping, several criteria can be chosen** and among them we can mention :

* **the instrument used** : ``ins_name``
* **the calibration file used** : ``cal_thfile``
* **the date night of the observation** : ``date_night``
* **a time interval** : ``binning_time``
* ...
* Or all of these at the same time.

On the `DACE website <https://dace.unige.ch>`_, it is already possible to perform binning and this functionality is also available in dace-query via ``algortihm.binning()``.

**Let's break down this function, what does it need to work?**

* **dataframe**, a pandas dataframe
* **group_by**, the grouping criteria as a list. *E.g*: ``['ins_name', 'date_night']``
* **binning_time**, (optional) a time interval for the grouping. ``binning_time=1`` corresponds to 24 hours, ``binning_time=1/86400`` corresponds to 1 second.

It is important to note that the following below columns must be present in the **dataframe**, as they are used by the clustering algorithm:

* **rjd** : the vector of barycentric reduced julian day. The unit must be **rBJD**.
* **rv_err** : the vector of errors on radial velocities
* **cal_therror** : the spectrogram wavelength calibration error vector. It can be a zero fully filled vector.

**Now let's look at an example of grouping data by instrument, by night and within a maximum of 0.7 days :**

.. code-block:: python

    from dace_query.spectroscopy import Spectroscopy
    from dace_query.helpers import algorithm

    # Retrieve the radial velocity data for the 51 Peg star as pandas dataframe
    raw_dataframe: pd.DataFrame = Spectroscopy.get_timeseries(
        target='51Peg',
        sorted_by_instrument=False,
        output_format='pandas')

    # Create a subset dataframe with important/chosen columns
    dataframe = raw_dataframe[
        ['rv', 'rv_err', 'fwhm', 'fwhm_err',
         'rjd', 'cal_therror', 'ins_name',
          'cal_thfile', 'date_night']].copy()

    # Fill NaN values with default values or drop NaN values,
    # ... this step is important to avoid computation error.
    dataframe['fwhm'].fillna(value=0.0, inplace=True)
    dataframe['fwhm_err'].fillna(value=0.0, inplace=True)

    # Define the group_by criteria, this must be valid columns names
    group_by = ['ins_name', 'date_night']
    # group_by = ['ins_name']
    # group_by = ['ins_name', 'cal_th_file', 'date_night'] # default


    # Define the time binning interval
    binning_time = 0.7

    # Compute the binned dataframe
    binned_df = algorithm.binning(
        dataframe=dataframe,
        group_by=group_by,
        binning_time=binning_time)

    # Voila, there is the binned dataframe available as a pandas.Dataframe
    binned_df

    # Now display the binned radial velocity data and its error
    print(binned_df[['rv', 'rv_err']].to_numpy())

    # Or simply get the binned radial velocity as a numpy.ndarray
    binned_rv = binned_df['rv'].to_numpy()

    # Note : By default, the indexes are "idx" and the "group_condition",
    # but they can be reset to use a more classical indices system.
    binned_df = binned_df.reset_index(drop=True)



