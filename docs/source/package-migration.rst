Migrate from `python-dace-client <https://pypi.org/project/python-dace-client/>`_ to `dace-query <https://pypi.org/project/dace-query/>`_
#########################################################################################################################################

This page was designed for users formerly using `python-dace-client` and who must now use `dace-query`.
It describes the main changes and thus allows a smooth transition for the use of `dace-query`.

Installation
************

As `dace-query` is a different python package from `python-dace-client` (and not an upgrade nor an update) it is therefore necessary to install it.

**Here is the pip install command :**

.. code-block:: bash

    # Moving from python-dace-client
    pip install python-dace-client

    # to dace-query
    pip install dace-query

Import
******

In order to avoid confusion (with `python-dace-client`) when using `dace-query` or moving to `dace-query`, the choice was made to name the module like the name of the python package, namely "dace-query".

**Here is the import command :**

.. code-block:: python

    # Moving from python-dace-client
    import dace

    # to dace-query
    import dace_query

Filters syntax
**************

The filters syntax is no longer completely the same between the `python-dace-client` package and the `dace-query` package, it has changed to **become more precise**.

The filter no longer accepts strings as before for certain operations, you must now give it an array of strings.

**Here is the list of filtering operations concerned by this change :**

* contains
* not contains
* equal
* not equal

**And here is the filters syntax :**

.. code-block:: python

    # Moving from python-dace-client
    filters: dict = {
        'obj_id_catname': {
            'contains': 'HD'
        }
    }


    # to dace-query
    filters: dict = {
        'obj_id_catname': {
            'contains': ['HD'] # !!!
        }
    }


To see all the filtering operations with associated examples, please consider visiting :doc:`query_options`.

