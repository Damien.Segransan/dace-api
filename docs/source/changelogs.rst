(dev) 1.2.0
***********
* Spectroscopy module
    * Get the guiding file for an observation point : ``Spectroscopy.get_guiding_file()``
    * Download multiples guiding files, added 'guiding' filetype : ``Spectroscopy.download_files()``
    * List & download (reduced) products : ``Spectroscopy.query_products()`` & ``Spectroscopy.download_product_files()``
* Helpers module added
    * Retrieve the proper motion and parallax value of a target : ``helpers.get_stellar_properties()``
    * Compute the secular acceleration : ``helpers.compute_secular_acceleration()``
    * Compute the binned dataframe : ``algorithm.binning()``
* Code improvements
* Enhanced documentation

1.1.0
*****

* Cheops module
    * List data products for a specified visit : ``Cheops.list_data_product()``
    * Download specific files using exact filenames: ``Cheops.download_files()``
* Fixes
    * Duplicated log issue
    * Download issue (with the accessing results method)
* Code improvements
* Enhanced documentation


1.0.1
*****

**The first public release of the dace-query package**

* Use `dace <https://dace.unige.ch>`_'s rest endpoints to retrieve datas
* Handle dace authentication using a **.dacerc** file
* New modules added
    * astrometry
    * atmosphericSpectroscopy
    * catalog
    * cheops
    * cheops
    * exoplanet
    * imaging
    * lossy
    * monitoring
    * opacity
    * opendata
    * photometry
    * population
    * spectroscopy
    * sun
    * target
    * tess
